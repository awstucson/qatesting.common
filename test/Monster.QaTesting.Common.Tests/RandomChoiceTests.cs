using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Monster.QaTesting.Common.Tests
{
    public class RandomChoiceTests
    {
        [Fact]
        public void EmptyIterable_Retuns_EmptyIterable()
        {
            var rndChoice = new RandomChoice(3);

            var emptyList = new List<string>();

            var resultEnumberable = rndChoice.SelectRandomChoices(emptyList);

            Assert.Empty(resultEnumberable);
        }

        [Fact]
        public void LessItems_Return_AllItems()
        {
            var rndChoice = new RandomChoice(3);

            var testList = new List<string> { "first" };

            var resultEnumerable = rndChoice.SelectRandomChoices(testList);

            Assert.Single(resultEnumerable);
        }

        [Fact]
        public void EqualItems_Return_AllItems()
        {
            var rndChoice = new RandomChoice(2);

            var testList = new List<string> { "first", "second" };

            var resultEnumerable = rndChoice.SelectRandomChoices(testList);

            Assert.Equal(2, resultEnumerable.Count());
        }

        [Fact]
        public void MoreItems_Return_RandomItems()
        {
            var rndChoice = new RandomChoice(2, 11);
            var testList = new List<string> { "first", "second", "third", "forth", "fifth" };
            var expectedList = new List<string> { "third", "first" };
            var returnEnumerable = rndChoice.SelectRandomChoices(testList);
            Assert.Equal(expectedList, returnEnumerable);
        }
    }
}
