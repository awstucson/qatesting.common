﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Monster.QaTesting.Common.Tasks
{
    public interface ITasksExecutor
    {
        Task ExecuteTasksAsync(IEnumerable<Task> tasks);
        void ExecuteTasks(IEnumerable<Task> tasks);
    }
}
