﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Monster.QaTesting.Common.Tasks
{
    public class MaxParallelTasksExecutor : ITasksExecutor
    {
        private readonly Task[] _taskList;

        private MaxParallelTasksExecutor(int maxTasks)
        {
            _taskList = new Task[maxTasks];
        }

        public async Task ExecuteTasksAsync(IEnumerable<Task> tasks)
        {
            InitializeTaskList();
            foreach (var task in tasks)
            {
                ExecuteTask(task);
            }
            await Task.WhenAll(_taskList);
        }

        public void ExecuteTasks(IEnumerable<Task> tasks)
        {
            ExecuteTasksAsync(tasks).Wait();
        }

        private void ExecuteTask(Task task)
        {
            var taskIndex = Task.WaitAny(_taskList);
            try
            {
                _taskList[taskIndex] = task;
            }
            catch (Exception ex)
            {
                Trace.WriteLine($"{ex}");
                throw;
            }
        }

        private void InitializeTaskList()
        {
            for (var i = 0; i < _taskList.Length; i++)
            {
                _taskList[i] = Task.FromResult(0);
            }
        }

        public static ITasksExecutor CreateInsTasksExecutor(int maxTasks)
        {
            return new MaxParallelTasksExecutor(maxTasks);
        }
    }
}