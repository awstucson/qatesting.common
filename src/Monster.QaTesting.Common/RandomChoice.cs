﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Monster.QaTesting.Common
{
    public class RandomChoice
    {
        private readonly int _numOfChoices;
        private readonly Random _rnd;

        public RandomChoice(int numOfChoices)
        {
            _numOfChoices = numOfChoices;
            _rnd = new Random();
        }

        public RandomChoice(int numOfChoices, int seed)
        {
            _numOfChoices = numOfChoices;
            _rnd = new Random(seed);
        }

        public IEnumerable<T> SelectRandomChoices<T>(IEnumerable<T> allEntries)
        {
            var allEntriesArray = allEntries.ToArray();
            var indexes = SelectIndexes(allEntriesArray.Length);
            foreach (var index in indexes)
            {
                yield return allEntriesArray[index];
            }
        }

        private IEnumerable<int> SelectIndexes(int count)
        {
            var selectedIndexes = new HashSet<int>();
            if (_numOfChoices == 0 || count <= _numOfChoices)
            {
                for (var i = 0; i < count; i++)
                {
                    selectedIndexes.Add(i);
                }
            }
            else
            {
                while (selectedIndexes.Count < _numOfChoices)
                {
                    selectedIndexes.Add(_rnd.Next(count));
                }
            }
            return selectedIndexes;
        }
    }
}
